//
//  NavigationController.swift
//  Ghostbiters
//
//  Created by Daniel Nissenbaum on 7/31/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: UINavigationController {
    
    override func shouldAutorotate() -> Bool {
        return self.visibleViewController.shouldAutorotate()
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return self.visibleViewController.supportedInterfaceOrientations()
    }
    
    
}