//
//  GameScene.swift
//  Ghostbiters
//
//  Created by Daniel Shaar on 7/1/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import SpriteKit
import CoreMotion
import GLKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // The scorebar height
    let SCOREBAR: CGFloat = 50
    // The border width
    let BORDER: CGFloat = 50
    // The maximum speed of the ghost
    let maxSpeed: CGFloat = 1000
    // The speed range of the ghost
    let speedRange: CGFloat = 800
    // The minimum speed of the ghost
    let minSpeed: CGFloat = 200
    // Margin for labels
    let MARGIN: CGFloat = 5
    // Boxman dimension
    let BOXMAN: CGFloat = 20
    // The powerup dimension
    let POWERUP: CGFloat = 25
    
    // An enum for bit masks
    enum GBPhysicsCategory:UInt32 {
        case GBBodyGhost = 1
        case GBBodyBoxman = 2
        case GBBodyPowerup = 4
        case GBBodyBounds = 8
    }
    
    // The ghost
    var ghost: SKSpriteNode = SKSpriteNode()
    // The labels for the score, countdown, and multiplier
    var scoreLabel: SKLabelNode = SKLabelNode()
    var countDownLabel: SKLabelNode = SKLabelNode()
    var countDownOutlineLabel: SKLabelNode = SKLabelNode()
    var multiplierLabel: SKLabelNode = SKLabelNode()
    // The labels for the lives display
    var livesLabel: SKLabelNode = SKLabelNode()
    var livesView: SKSpriteNode = SKSpriteNode()
    
    // The score and multiplier
    var score: Double = 0.0
    var multiplier: Double = 1.0
    
    // The number of lives
    var lives: Int = 3
    
    // Is the ghost facing left
    var facingLeft: Bool = true
    
    // Motion manager
    var motionManager: CMMotionManager = CMMotionManager()
    // Last vector from accelerometer data
    var lastVector: [Double] = [0.0, 0.0, 0.0]
    
    // Are we using joystick or accelerometer
    var joystick: Bool = NSUserDefaults().boolForKey("joystickSelected");
    var joystickControl: Joystick = Joystick(frame: CGRectZero)
    
    // The time between enemy spawns and powerup spawns
    var spawnTime: NSTimeInterval = 2.0
    var powerupTime: NSTimeInterval = 6.0
    
    // The time it takes for a basic boxman to move across the screen
    var basicEnemyTime: NSTimeInterval = 4.0
    
    // The basic enemy animation
    var animation: [SKAction] = []
    
    // Is the game over
    var gameOverView: UIView = UIView()
    var finalScoreLabel:UILabel = UILabel()
    var highScoreLabel:UILabel = UILabel()
    //var scrollView:UIScrollView = UIScrollView()
    //var loginView:LoginView = LoginView()
    var pauseMenu: UIView = UIView()
    
    
    // Have we started
    var started: Bool = false
    var startTime: NSTimeInterval = 0
    
    // The enemies
    var enemies: [SKSpriteNode] = []
    var powerups: [SKSpriteNode] = []
    let powerupNames = ["armor", "doubleScore", "halfScore", "nuke", "oneUp", "slowdown", "speedup"]
    
    // Is the ghost immune
    var immunity: Bool = false
    
    // Should the ghost flicker
    var flickerImmunity: Bool = false
    
    // ay vector for accelerometer
    //var ay = [0.54, 0.0, -0.84];
    var pos = true
    
    // Sensitivity of accelerometer
    var horizontalSensitivity: CGFloat = 0.5
    var verticalSensitivity: CGFloat = 0.5
    
    // Game settings
    var settings = NSUserDefaults()
    
    var firstTry = true
    
    // Initializes the scene
    override init(size: CGSize) {
        
        super.init(size: size)
        
        joystick = settings.boolForKey("joystickSelected");
        
        // The center of the screen
        var center: CGPoint = CGPointMake(self.size.width / 2, self.size.height / 2)
        
        // Initializes the background
        var background: SKSpriteNode = SKSpriteNode(imageNamed: "Background")
        background.position = center
        self.addChild(background)
        
        // Initializes the labels
        setUpGameUI(background)
        
        // Initializes the ghost
        ghost = SKSpriteNode(imageNamed: "ghost")
        ghost.zPosition = 100
        ghost.position = center
        ghost.physicsBody = SKPhysicsBody(circleOfRadius: ghost.frame.size.width / 2.2)
        ghost.physicsBody?.categoryBitMask = GBPhysicsCategory.GBBodyGhost.toRaw()
        ghost.physicsBody?.contactTestBitMask = GBPhysicsCategory.GBBodyBoxman.toRaw()
        ghost.physicsBody?.collisionBitMask = GBPhysicsCategory.GBBodyBounds.toRaw()
        ghost.physicsBody?.allowsRotation = false
        self.addChild(ghost)
        
        // Sets up animation for boxman
        let textureAtlas = SKTextureAtlas(named: "sprites.atlas")
        
        var textures = Array<Array<SKTexture>>()
        
//        for var i = 0; i < 5; i++ {
//            var tempArray = Array<SKTexture>()
//            for var j = 1; j < 7; j++ {
//                tempArray.append(textureAtlas.textureNamed(NSString(format: "boxman%d_%d", i + 1, j)))
//            }
//            for var j = 7; j > 1; j-- {
//                tempArray.append(textureAtlas.textureNamed(NSString(format: "boxman%d_%d", i + 1, j)))
//            }
//            textures.append(tempArray)
//            animation.append(SKAction.animateWithTextures(textures[i], timePerFrame: 0.1))
//        }
        for var i = 0; i < 5; i++ {
            var tempArray = Array<SKTexture>()
            tempArray.append(textureAtlas.textureNamed(NSString(format: "boxman%d_1", i + 1)))
            textures.append(tempArray)
            animation.append(SKAction.animateWithTextures(textures[i], timePerFrame: 0.1))
        }
        
        // Sets up the physics of the screen
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: CGRectMake(BORDER, BORDER, self.size.width - 2 * BORDER, self.size.height - SCOREBAR - 2 * BORDER))
        self.physicsBody?.categoryBitMask = GBPhysicsCategory.GBBodyBounds.toRaw()
        self.physicsBody?.dynamic = false
        self.physicsWorld.contactDelegate = self
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    // Sets up the labels
    func setUpGameUI(background: SKSpriteNode) {
        
        // The score label
        scoreLabel = SKLabelNode(fontNamed: "TechnoHideo")
        scoreLabel.text = String(Int(score))
        scoreLabel.fontSize = 40
        scoreLabel.fontColor = SKColor.whiteColor()
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        scoreLabel.position = CGPointMake(background.frame.size.width - scoreLabel.frame.size.width - 2 * MARGIN,
            background.frame.size.height - scoreLabel.frame.size.height - 1.5 * MARGIN)
        self.addChild(scoreLabel)
        
        
        // The multiplier label
        multiplierLabel = SKLabelNode(fontNamed: "TechnoHideo")
        multiplierLabel.text = "X" + NSString(format:"%.1f", multiplier)
        multiplierLabel.fontSize = 18
        multiplierLabel.fontColor = SKColor.orangeColor()
        multiplierLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        multiplierLabel.position = CGPointMake(background.frame.size.width - multiplierLabel.frame.size.width - 2 * MARGIN,
            background.frame.size.height - multiplierLabel.frame.size.height - scoreLabel.frame.size.height - 2 * MARGIN)
        self.addChild(multiplierLabel)
        
        updateLives()
        
        // The countdown label
        countDownOutlineLabel = SKLabelNode(fontNamed: "Technohideo")
        countDownOutlineLabel.text = "3"
        countDownOutlineLabel.fontSize = 500
        countDownOutlineLabel.fontColor = SKColor.blackColor()
        countDownOutlineLabel.position = CGPointMake(background.frame.size.width / 2, background.frame.size.height / 2 - 60)
        self.addChild(countDownOutlineLabel)
        
        // The countdown label
        countDownLabel = SKLabelNode(fontNamed: "Technohideo")
        countDownLabel.text = "3"
        countDownLabel.zPosition = 20
        countDownLabel.fontSize = 400
        countDownLabel.fontColor = SKColor.whiteColor()
        countDownLabel.position = CGPointMake(background.frame.size.width / 2, background.frame.size.height / 2 - 30)
        self.addChild(countDownLabel)
    }
    
    // Updates the lives
    func updateLives() {
        // Resets the lives view and label
        if livesView.parent != nil {
            livesView.removeFromParent()
        }
        if livesLabel.parent != nil {
            livesLabel.removeFromParent()
        }
        
        if lives <= 0 {
            return
        }
        
        // If the number of lives is greater than 5, we display [image]X[lives]
        if lives > 5 {
            // Sets up the image
            livesView = SKSpriteNode(imageNamed: "life_1")
            livesView.anchorPoint = CGPointMake(0, 1)
            livesView.position = CGPointMake(MARGIN, self.frame.size.height - 1.5 * MARGIN)
            self.addChild(livesView)
            
            // Sets up the label
            livesLabel = SKLabelNode(fontNamed: "TechnoHideo")
            livesLabel.text = "X" + String(lives)
            livesLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
            livesLabel.fontSize = 45;
            livesLabel.fontColor = UIColor.orangeColor()
            livesLabel.position = CGPointMake(livesLabel.frame.size.width + livesView.frame.size.width + 2 * MARGIN, self.frame.size.height - livesView.frame.size.height - MARGIN)
            self.addChild(livesLabel)
        }
            // If the number of lives is 5 or less, we display the image
        else {
            // Sets up the image
            livesView = SKSpriteNode(imageNamed: "life_" + String(lives))
            livesView.anchorPoint = CGPointMake(0, 1)
            livesView.position = CGPointMake(MARGIN, self.frame.size.height - 1.5 * MARGIN)
            self.addChild(livesView)
        }
    }
    
    // Updates the color and text of the multiplier label
    func updateMultiplier() {
        multiplierLabel.text = "X" + NSString(format:"%.1f", multiplier)
        if multiplier < 1.0 {
            multiplierLabel.fontColor = SKColor.redColor()
        } else if multiplier == 1.0 {
            multiplierLabel.fontColor = SKColor.orangeColor()
        } else {
            multiplierLabel.fontColor = SKColor.greenColor()
        }
    }
    
    // Updates the scene
    override func update(currentTime: NSTimeInterval) {
        // Makes the countdown
        if !started {
            var orientation = UIDevice.currentDevice().orientation
            if orientation == UIDeviceOrientation.LandscapeLeft {
                
                if settings.doubleForKey("ayx") > 0 {
                    settings.setDouble(settings.doubleForKey("ayx") * -1, forKey: "ayx")
                }
                
                pos = false
            } else if orientation == UIDeviceOrientation.LandscapeRight {
                
                if settings.doubleForKey("ayx") < 0 {
                    settings.setDouble(settings.doubleForKey("ayx") * -1, forKey: "ayx")
                }
                
                pos = true
            }
            
            startTime = currentTime
            if firstTry {
                startTime += 0.75
                firstTry = false
            }
            started = true
            
            // Counts down until game start and then spawns boxmen
            self.runAction(SKAction.sequence([
                SKAction.waitForDuration(3.8),
                SKAction.runBlock(self.recursiveActionMethod)]), withKey: "spawn")
            
            // Makes powerups
            self.runAction(SKAction.sequence([
                SKAction.waitForDuration(8.0),
                SKAction.repeatActionForever(SKAction.sequence([
                    SKAction.runBlock(self.updatePowerups),
                    SKAction.waitForDuration(powerupTime)
                    ]))]), withKey: "powerups")
            
            // Counts down until game start and then spawns points
            self.runAction(SKAction.sequence([
                SKAction.waitForDuration(3.8),
                SKAction.repeatActionForever(SKAction.sequence([
                    SKAction.runBlock({self.score += 7.0 * self.multiplier
                        self.scoreLabel.text = String(Int(self.score))
                        self.scoreLabel.position = CGPointMake(self.frame.size.width - self.scoreLabel.frame.size.width - 2 * self.MARGIN, self.frame.size.height - self.scoreLabel.frame.size.height - 1.5 * self.MARGIN)}),
                    SKAction.waitForDuration(0.2)
                    ]))]), withKey: "scoreUp")
        }
        
        var time = currentTime - startTime
        if time >= 3.6 {
            countDownLabel.removeFromParent()
            countDownOutlineLabel.removeFromParent()
        } else if time <= 3 {
            var transparent = CGFloat(1.0 - time + Double(Int(time)))
            countDownLabel.fontColor = SKColor(red: 1.0, green: 1.0, blue: 1.0, alpha: transparent)
            countDownLabel.text = "\(3 - Int(time))"
            countDownOutlineLabel.fontColor = SKColor(red: 0.0, green: 0.0, blue: 0.0, alpha: transparent)
            countDownOutlineLabel.text = "\(3 - Int(time))"
        } else {
            var transparent = CGFloat(1.0 - time + Double(Int(time)))
            countDownLabel.text = "GO!"
            countDownLabel.fontColor = SKColor(red: 1.0, green: 1.0, blue: 1.0, alpha: transparent)
            countDownOutlineLabel.fontSize = 410
            countDownOutlineLabel.fontColor = SKColor.blackColor()
            countDownOutlineLabel.fontColor = SKColor(red: 0.0, green: 0.0, blue: 0.0, alpha: transparent)
            countDownOutlineLabel.text = "GO!"
        }
        
        
        if !joystick {
            moveGhost(CGPointZero)
        }
    }
    
    // Moves the ghost
    func moveGhost(relativePosition: CGPoint) {
        var rotationAngle: CGFloat = 0
        
        verticalSensitivity = CGFloat(settings.doubleForKey("verticalSensitivity"))
        horizontalSensitivity = CGFloat(settings.doubleForKey("horizontalSensitivity"))
        
        // Sets the speed
        if joystick {
            ghost.physicsBody?.velocity = CGVectorMake(relativePosition.x * (minSpeed + speedRange * horizontalSensitivity), relativePosition.y * (minSpeed + speedRange * verticalSensitivity))
            
            // Sets the direction of the ghost with limited rotation
            if !CGPointEqualToPoint(relativePosition, CGPointZero) {
                // The rotation angle of the joystick
                rotationAngle = CGFloat(atan2f(CFloat(relativePosition.y), CFloat(relativePosition.x)))
            }
        } else if motionManager.accelerometerData != nil {
            // Raw acceleration vector
            var raw = [motionManager.accelerometerData.acceleration.x,
                motionManager.accelerometerData.acceleration.y,
                motionManager.accelerometerData.acceleration.z]
            
            raw = lowPass(&raw)
            
            // If there is no motion, stop
            if raw[0] == 0 && raw[1] == 0 && raw[2] == 0 {
                return;
            }
            
            var ay: [Double] = [settings.doubleForKey("ayx"), settings.doubleForKey("ayy"), settings.doubleForKey("ayz")];
            //NSLog("\(ay[2])")
            
            
            // The neutral position
            var az = [0.0, 1.0, 0.0]
            var ax = [ay[2], 0.0, -ay[0]]
            
            var rawDotAz = raw[0] * az[0]
            rawDotAz += raw[1] * az[1]
            rawDotAz += raw[2] * az[2]
            
            var rawDotAx = raw[0] * ax[0]
            rawDotAx += raw[1] * ax[1]
            rawDotAx += raw[2] * ax[2]
            
            // The motion vector
            var accel2D = CGPointZero
            if pos {
                accel2D = CGPointMake(CGFloat(rawDotAz), CGFloat(rawDotAx))
            } else {
                accel2D = CGPointMake(CGFloat(-rawDotAz), CGFloat(-rawDotAx))
            }
            
            var steerDeadZone: CGFloat = 0.02
            
            // Filters out little jitters
            if accel2D.x < steerDeadZone && accel2D.x > -steerDeadZone {
                accel2D.x = 0
            }
            if accel2D.y < steerDeadZone && accel2D.y > -steerDeadZone {
                accel2D.y = 0
            }
            
            
            ghost.physicsBody?.velocity = CGVectorMake(2.5 * accel2D.x * (minSpeed + speedRange * horizontalSensitivity), 2.5 * accel2D.y * (minSpeed + speedRange * verticalSensitivity))
            
            // Sets the direction of the ghost with limited rotation
            if !CGPointEqualToPoint(accel2D, CGPointZero) {
                rotationAngle = CGFloat(atan2f(CFloat(accel2D.y), CFloat(accel2D.x)))
            }
        }
        
        // Sets the rotation of the ghost
        if rotationAngle <= CGFloat(M_PI_2) && rotationAngle > -CGFloat(M_PI_2) {
            if facingLeft {
                facingLeft = false
            }
            ghost.yScale = 1.0
            ghost.xScale = -1.0
            ghost.zRotation = rotationAngle / 3
        } else if rotationAngle > CGFloat(M_PI_2) && rotationAngle <= CGFloat(M_PI) {
            if !facingLeft {
                facingLeft = true
            }
            ghost.xScale = -1.0
            ghost.yScale = -1.0
            ghost.zRotation = 2 * CGFloat(M_PI) / 3 + rotationAngle / 3
        } else {
            if !facingLeft {
                facingLeft = true
            }
            ghost.xScale = -1.0
            ghost.yScale = -1.0
            ghost.zRotation = -2 * CGFloat(M_PI) / 3 + rotationAngle / 3
        }
    }
    
    // Damps the gittery motion with a lowpass filter
    func lowPass(inout vector: [Double]) -> [Double] {
        let blend: Double = 0.2
        
        // Smoothens out the data input
        vector[0] = vector[0] * blend + lastVector[0] * (1 - blend)
        vector[1] = vector[1] * blend + lastVector[1] * (1 - blend)
        vector[2] = vector[2] * blend + lastVector[2] * (1 - blend)
        
        // Sets the last vector to be the current one
        lastVector = vector
        
        // Returns the lowpass vector
        return vector
    }
    
    // Spawns a basic enemy
    func spawnBasicEnemy() {
        
        var enemyNum = Int(arc4random_uniform(100))
        // The enemy with starting image
        var enemy = SKSpriteNode()
        
        if enemyNum < 20 {
            enemy = SKSpriteNode(texture: SKTexture(imageNamed: "boxman2_1"))
        } else {
            enemy = SKSpriteNode(texture: SKTexture(imageNamed: "boxman1_1"))
        }
        
        // Places the enemy in one of four zones
        let startingRegion = arc4random_uniform(4)
        var endPoint = CGPointZero
        var rotationAngle = CGFloat(0)
        var upDown = SKAction()
        var side = SKAction()
        
        switch startingRegion {
        case 0:
            var y = BORDER + BOXMAN + CGFloat(Int(arc4random_uniform(578)))
            rotationAngle = CGFloat(M_PI_2)
            enemy.position = CGPointMake(BORDER / 2, y)
            endPoint = CGPointMake(self.size.width - BORDER / 2, y)
            upDown = SKAction.sequence([
                SKAction.moveBy(CGVectorMake(0, 80), duration: basicEnemyTime / 3),
                SKAction.moveBy(CGVectorMake(0, -160), duration: basicEnemyTime / 3),
                SKAction.moveBy(CGVectorMake(0, 80), duration: basicEnemyTime / 3)
                ])
            side = SKAction.moveBy(CGVectorMake(self.size.width - BORDER, 0), duration: basicEnemyTime)
        case 1:
            var x = BORDER + BOXMAN + CGFloat(Int(arc4random_uniform(884)))
            rotationAngle = CGFloat(M_PI)
            enemy.position = CGPointMake(x, BORDER / 2)
            endPoint = CGPointMake(x, self.size.height - 3 * BORDER / 2)
            upDown = SKAction.sequence([
                SKAction.moveBy(CGVectorMake(80, 0), duration: basicEnemyTime / 3),
                SKAction.moveBy(CGVectorMake(-160, 0), duration: basicEnemyTime / 3),
                SKAction.moveBy(CGVectorMake(80, 0), duration: basicEnemyTime / 3)
                ])
            side = SKAction.moveBy(CGVectorMake(0, self.size.height - 2 * BORDER), duration: basicEnemyTime)
        case 2:
            var y = BORDER + BOXMAN + CGFloat(Int(arc4random_uniform(578)))
            rotationAngle = CGFloat(-M_PI_2)
            enemy.position = CGPointMake(self.size.width - BORDER / 2, y)
            endPoint = CGPointMake(BORDER / 2, y)
            upDown = SKAction.sequence([
                SKAction.moveBy(CGVectorMake(0, -80), duration: basicEnemyTime / 3),
                SKAction.moveBy(CGVectorMake(0, 160), duration: basicEnemyTime / 3),
                SKAction.moveBy(CGVectorMake(0, -80), duration: basicEnemyTime / 3)
                ])
            side = SKAction.moveBy(CGVectorMake(BORDER - self.size.width, 0), duration: basicEnemyTime)
        case 3:
            var x = BORDER + BOXMAN + CGFloat(Int(arc4random_uniform(884)))
            enemy.position = CGPointMake(x, self.size.height - 3 * BORDER / 2)
            endPoint = CGPointMake(x, BORDER / 2)
            upDown = SKAction.sequence([
                SKAction.moveBy(CGVectorMake(-80, 0), duration: basicEnemyTime / 3),
                SKAction.moveBy(CGVectorMake(160, 0), duration: basicEnemyTime / 3),
                SKAction.moveBy(CGVectorMake(-80, 0), duration: basicEnemyTime / 3)
                ])
            side = SKAction.moveBy(CGVectorMake(0, 2 * BORDER - self.size.height), duration: basicEnemyTime)
        default:
            return
        }
        
        // Set enemy collision detection
        enemy.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(1.5 * BOXMAN, 1.5 * BOXMAN))
        enemy.physicsBody?.categoryBitMask = GBPhysicsCategory.GBBodyBoxman.toRaw()
        enemy.physicsBody?.contactTestBitMask = GBPhysicsCategory.GBBodyGhost.toRaw()
        enemy.physicsBody?.collisionBitMask = 0
        enemy.physicsBody?.allowsRotation = false
        enemy.zPosition = 20
        enemy.alpha = CGFloat(0.0)
        enemy.size = CGSizeMake(0.0, 0.0)
        
        // Adds the enemy
        self.addChild(enemy)
        enemies.append(enemy)
        
        if enemyNum < 15 {
            // Moves the enemy to the endzone
            enemy.runAction(SKAction.group([
                SKAction.sequence([
                    SKAction.rotateByAngle(rotationAngle, duration:0.01),
                    SKAction.group([SKAction.rotateByAngle(31.4, duration: 1),
                            SKAction.fadeInWithDuration(1),
                            SKAction.resizeToWidth(CGFloat(45.0), height: CGFloat(45.0), duration: 1)]),
                    SKAction.group([
                        side,
                        upDown
                        ]),
                    SKAction.group([SKAction.rotateByAngle(31.4, duration: 1),
                        SKAction.fadeOutWithDuration(1),
                        SKAction.resizeToWidth(CGFloat(0.0), height: CGFloat(0.0), duration: 1)]),
                    SKAction.removeFromParent()
                    ]),
                SKAction.repeatActionForever(animation[1])
                ]))
        } else {
            // Moves the enemy to the endzone
            enemy.runAction(SKAction.group([
                SKAction.sequence([
                    SKAction.rotateByAngle(rotationAngle, duration:0.01),
                    SKAction.group([SKAction.rotateByAngle(31.4, duration: 1),
                            SKAction.fadeInWithDuration(1),
                            SKAction.resizeToWidth(CGFloat(45.0), height: CGFloat(45.0), duration: 1)]),
                    SKAction.moveTo(endPoint, duration:basicEnemyTime),
                    SKAction.group([SKAction.rotateByAngle(31.4, duration: 1),
                        SKAction.fadeOutWithDuration(1),
                        SKAction.resizeToWidth(CGFloat(0.0), height: CGFloat(0.0), duration: 1)]),
                    SKAction.removeFromParent()
                    ]),
                SKAction.repeatActionForever(animation[0])
                ]))
        }
        
    }
    
    func recursiveActionMethod() {
        if spawnTime < 0.2 {
            spawnTime -= 0.00005
        } else if spawnTime < 0.3 {
            spawnTime -= 0.0001
        } else if spawnTime < 0.4 {
            spawnTime -= 0.0010
        } else if spawnTime < 0.5 {
            spawnTime -= 0.0050
        } else if spawnTime < 0.6 {
            spawnTime -= 0.0100
        } else if spawnTime < 0.7 {
            spawnTime -= 0.0150
        } else if spawnTime < 1.0 {
            spawnTime -= 0.0200
        } else if spawnTime < 1.3 {
            spawnTime -= 0.0300
        } else if spawnTime < 1.6 {
            spawnTime -= 0.0450
        } else if spawnTime >= 1.6 {
            spawnTime -= 0.0650
        }
    
        // store duration in a property
        var waitAction = SKAction.waitForDuration(spawnTime)
        var theAction = SKAction.runBlock(self.spawnBasicEnemy)
        var recursiveAction = SKAction.runBlock(self.recursiveActionMethod)
    
        var sequence = SKAction.sequence([waitAction, theAction, recursiveAction])
        self.runAction(sequence)
    }
    
    // Updates the powerups on screen
    func updatePowerups() {
        
        // Randomly generates the powerups with appropriate probabilities
        var powerupRand = arc4random_uniform(100)
        var powerup: SKSpriteNode
        
        if powerupRand < 10 {
            powerup = SKSpriteNode(imageNamed: powerupNames[0])
            powerup.name = powerupNames[0]
        } else if powerupRand < 25 {
            powerup = SKSpriteNode(imageNamed: powerupNames[1])
            powerup.name = powerupNames[1]
        } else if powerupRand < 40 {
            powerup = SKSpriteNode(imageNamed: powerupNames[2])
            powerup.name = powerupNames[2]
        } else if powerupRand < 48 {
            powerup = SKSpriteNode(imageNamed: powerupNames[3])
            powerup.name = powerupNames[3]
        } else if powerupRand < 56 {
            powerup = SKSpriteNode(imageNamed: powerupNames[4])
            powerup.name = powerupNames[4]
        } else if powerupRand < 76 {
            powerup = SKSpriteNode(imageNamed: powerupNames[5])
            powerup.name = powerupNames[5]
        } else {
            powerup = SKSpriteNode(imageNamed: powerupNames[6])
            powerup.name = powerupNames[6]
        }
        
        // Random position
        var x = CGFloat(Double(50 + arc4random_uniform(924)))
        var y = CGFloat(Double(50 + arc4random_uniform(618)))
        powerup.position = CGPointMake(x, y)
        
        // Sets up collision detection physics
        powerup.physicsBody = SKPhysicsBody(circleOfRadius: POWERUP)
        powerup.physicsBody?.categoryBitMask = GBPhysicsCategory.GBBodyPowerup.toRaw()
        powerup.physicsBody?.contactTestBitMask = GBPhysicsCategory.GBBodyGhost.toRaw()
        powerup.physicsBody?.collisionBitMask = GBPhysicsCategory.GBBodyBounds.toRaw()
        self.addChild(powerup)
        
        // Adds the powerup to the powerup array
        powerups.append(powerup)
        
        // Animation
        var fullWiggle = SKAction.sequence([
            SKAction.rotateByAngle(CGFloat(M_PI) / 8, duration: 0.5),
            SKAction.rotateByAngle(-CGFloat(M_PI) / 8, duration: 0.5)])
        var fullScale = SKAction.sequence([
            SKAction.scaleBy(1.2, duration: 0.25),
            SKAction.scaleBy(5 / 6, duration: 0.25),
            SKAction.scaleBy(1.2, duration: 0.25),
            SKAction.scaleBy(5 / 6, duration: 0.25)
            ])
        
        powerup.runAction(SKAction.repeatActionForever(SKAction.group([fullWiggle, fullScale])))
        powerup.runAction(SKAction.sequence([
            SKAction.waitForDuration(9.0),
            SKAction.removeFromParent()]))
    }
    
    // Detects collisions and deals with them
    func didBeginContact(contact: SKPhysicsContact!) {
        var collision: UInt32 = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        // Enemy and ghost collision
        if collision == GBPhysicsCategory.GBBodyGhost.toRaw() | GBPhysicsCategory.GBBodyBoxman.toRaw() {
            // If the ghost is flickering, stop detecting collisions
            if flickerImmunity {
                return
                // Otherwise, turn on flickering
            } else {
                flickerImmunity = true
                ghost.runAction(SKAction.sequence([
                    SKAction.repeatAction(SKAction.sequence([
                        SKAction.fadeAlphaTo(0.5, duration: 0.2),
                        SKAction.fadeAlphaTo(1.0, duration: 0.2)]), count: 5),
                    SKAction.runBlock({self.flickerImmunity = false})]), withKey: "flicker")
                if immunity {
                    immunity = false
                    self.removeActionForKey("Armor")
                    ghost.runAction(SKAction.setTexture(SKTexture(imageNamed: "ghost")))
                } else {
                    lives--
                    updateLives()
                    if lives == 0 {
                        endGame()
                    } else {
                        contact.bodyA.node?.removeFromParent()
                    }
                }
            }
            // Ghost and powerup collision
        } else if collision == GBPhysicsCategory.GBBodyPowerup.toRaw() | GBPhysicsCategory.GBBodyGhost.toRaw() {
            if contact.bodyB.node?.name == powerupNames[0] {
                if self.actionForKey("Armor") != nil {
                    self.removeActionForKey("Armor")
                }
                endFlicker()
                ghost.runAction((SKAction.sequence([
                    SKAction.setTexture(SKTexture(imageNamed: "armor_ghost")),
                    SKAction.runBlock({self.immunity = true}),
                    SKAction.waitForDuration(12),
                    SKAction.setTexture(SKTexture(imageNamed: "ghost")),
                    SKAction.runBlock({self.immunity = false})])), withKey: "Armor")
            } else if contact.bodyB.node?.name == powerupNames[1] {
                self.runAction(SKAction.sequence([
                    SKAction.runBlock({self.multiplier *= 2
                        self.updateMultiplier()}),
                    SKAction.waitForDuration(8),
                    SKAction.runBlock({self.multiplier /= 2
                        self.updateMultiplier()})
                    ]))
            } else if contact.bodyB.node?.name == powerupNames[2] {
                self.runAction(SKAction.sequence([
                    SKAction.runBlock({self.multiplier /= 2
                        self.updateMultiplier()}),
                    SKAction.waitForDuration(8),
                    SKAction.runBlock({self.multiplier *= 2
                        self.updateMultiplier()})
                    ]))
            } else if contact.bodyB.node?.name == powerupNames[3] {
                var nukePath: String = NSBundle.mainBundle().pathForResource("Nuke", ofType: "sks")!
                var nukeEmitter: SKEmitterNode = NSKeyedUnarchiver.unarchiveObjectWithFile(nukePath) as SKEmitterNode
                nukeEmitter.position = CGPointMake(0, 0)
                ghost.addChild(nukeEmitter)
                nukeEmitter.runAction(SKAction.sequence([SKAction.waitForDuration(1.0), SKAction.removeFromParent()]))
                for node in enemies {
                    //if node != nil {
                    node.removeFromParent()
                    score += multiplier * 17
                    //}
                }
//                self.runAction(SKAction.sequence([
//                    SKAction.waitForDuration(1.0),
//                    SKAction.sequence([
//                        SKAction.waitForDuration(3.8),
//                        SKAction.repeatActionForever(SKAction.sequence([
//                            SKAction.runBlock(self.spawnBasicEnemy),
//                            SKAction.waitForDuration(spawnTime)
//                            ]))])]), withKey: "spawn")
            } else if contact.bodyB.node?.name == powerupNames[4] {
                lives++
                updateLives()
            } else if contact.bodyB.node?.name == powerupNames[5] {
                self.runAction(SKAction.sequence([
                    SKAction.runBlock({self.basicEnemyTime *= 1.4}),
                    SKAction.waitForDuration(8),
                    SKAction.runBlock({self.basicEnemyTime *= 0.7})]), withKey: "slowdown")
            } else {
                self.runAction(SKAction.sequence([
                    SKAction.runBlock({self.basicEnemyTime *= 0.8}),
                    SKAction.waitForDuration(12),
                    SKAction.runBlock({self.basicEnemyTime *= 1.25})]), withKey: "speedup")
            }
            contact.bodyB.node?.removeFromParent()
        }
    }
    
    // Ends the game
    func endGame() {
        
        joystick = settings.boolForKey("joystickSelected");
        
        // Removes the motion.
        if !joystick {
            motionManager.stopAccelerometerUpdates()
        } else {
            joystickControl.hidden = true
        }
        
        // Hides all characters on screen
        ghost.hidden = true
        ghost.physicsBody?.dynamic = false
        scoreLabel.hidden = true
        multiplierLabel.hidden = true
        self.removeAllActions()
        endFlicker()
//        for enemy in enemies {
//            //if enemy != nil {
//            enemy.removeFromParent()
//            //}
//        }
        for powerup in powerups {
            //if powerup != nil {
            powerup.removeFromParent()
            //}
        }
        
        // Display final score
        finalScoreLabel.text = "Score:  \(Int(score))"
        
        // Get path of score file and load scores array
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        var path = paths[0].stringByAppendingPathComponent("scores")
        var scores = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as [Score]
        
        var scrobj = Score(scr: Int(score))
        
        // insert score in correct place
        var inserted = false;
        for var i = 0; i < scores.count; i++ {
            if (scores[i].score < Int(score)) {
                scores.insert(scrobj, atIndex: i)
                inserted = true;
                break
            }
        }
        
        if !inserted  {
            scores.append(scrobj);
        }
        
        
        NSKeyedArchiver.archiveRootObject(scores, toFile: path)
        
        var highScore = settings.doubleForKey("highScore")
        

        
        if highScore < score {
            settings.setDouble(score, forKey: "highScore")
            
//            if (PFUser.currentUser() != nil) {
//                var val = NSNumber(double: score)
//            
//                PFUser.currentUser().setObject(val, forKey: "highScore")
//                PFUser.currentUser().saveInBackground()
//            }
            highScore = score
        }
        
        // Sets scoreboard
//        if FBSession.activeSession().isOpen {
//            scrollView.hidden = false;
//            loginView.hidden = true;
//            
//        } else {
//            scrollView.hidden = true;
//            loginView.hidden = false;
//        }
        
        highScoreLabel.text = "High Score:  \(Int(highScore))"
        gameOverView.hidden = false
        
        settings.setBool(true, forKey: "rotateAllowed")
        settings.synchronize()
    }
    
    // Stops the flicker immunity
    func endFlicker() {
        if flickerImmunity {
            flickerImmunity = false
            ghost.removeActionForKey("flicker")
            ghost.runAction(SKAction.fadeAlphaTo(1.0, duration: 0.01))
        }
    }
    
    // Resets the game
    func reset() {
        lives = 3
        updateLives()
        
        // checks the settings fo the joystick
        joystick = settings.boolForKey("joystickSelected")
        if !joystick {
            motionManager.startAccelerometerUpdates()
        } else {
            joystickControl.hidden = false
        }
        
        for enemy in enemies {
            //if enemy != nil {
            enemy.removeFromParent()
            //}
        }
        for powerup in powerups {
            //if powerup != nil {
            powerup.removeFromParent()
            //}
        }
        
        ghost.physicsBody?.dynamic = true
        score = 0.0
        multiplier = 1.0
        updateMultiplier()
        self.scoreLabel.text = String(Int(self.score))
        self.scoreLabel.position = CGPointMake(self.frame.size.width - self.scoreLabel.frame.size.width - 2 * self.MARGIN, self.frame.size.height - self.scoreLabel.frame.size.height - 1.5 * self.MARGIN)
        ghost.hidden = false
        ghost.position = CGPointMake(self.size.width / 2, self.size.height / 2)
        scoreLabel.hidden = false
        multiplierLabel.hidden = false
        self.addChild(countDownOutlineLabel)
        self.addChild(countDownLabel)
        gameOverView.hidden = true
        started = false
        settings.setBool(false, forKey: "rotateAllowed")
        spawnTime = 2.0;
    }
    
    // Pauses gameplay.
    func pause() {
        // Removes the motion.
        if !joystick {
            motionManager.stopAccelerometerUpdates()
        } else {
            joystickControl.hidden = true
        }
        pauseMenu.hidden = false
        self.removeAllActions()
        for enemy in enemies {
            //if enemy != nil {
                enemy.paused = true
            //}
        }
        for powerup in powerups {
            //if powerup != nil {B
            powerup.paused = true
            //}
        }

        endFlicker()
        ghost.physicsBody?.dynamic = false
        settings.setBool(true, forKey: "rotateAllowed")
        //if countDownLabel != nil {
            countDownLabel.removeFromParent()
        //}
        //if countDownOutlineLabel != nil {
            countDownOutlineLabel.removeFromParent()
        //}
    }
    
    // Performs the resuming after the countdown.
    func unpause() {
        // Resumes the motion.
        joystick = settings.boolForKey("joystickSelected")
        if !joystick {
            motionManager.startAccelerometerUpdates()
        } else {
            joystickControl.hidden = false
        }
        for enemy in enemies {
            //if enemy != nil {
                enemy.paused = false
            //}
        }
        for powerup in powerups {
            //if powerup != nil {B
                powerup.paused = false
            //}
        }
        ghost.physicsBody?.dynamic = true
    }
    
    // Resumes gameplay.
    func resume() {
        settings.setBool(false, forKey: "rotateAllowed")
        started = false
        pauseMenu.hidden = true
        joystick = settings.boolForKey("joystickSelected")
        if !joystick {
            motionManager.startAccelerometerUpdates()
        } else {
            joystickControl.hidden = false
        }
        self.addChild(countDownOutlineLabel)
        self.addChild(countDownLabel)
        self.runAction(SKAction.sequence([
            SKAction.waitForDuration(3),
            SKAction.runBlock(self.unpause)]))
    }
}
