//
//  CalibrationScene.swift
//  Ghostbiters
//
//  Created by Daniel Nissenbaum on 8/12/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import Foundation
import SpriteKit
import CoreMotion
import GLKit

class CalibrationScene: SKScene, SKPhysicsContactDelegate {
    
    // The border width
    let BORDER: CGFloat = 50
    // The maximum speed of the ghost
    let maxSpeed: CGFloat = 1000
    // The speed range of the ghost
    let speedRange: CGFloat = 800
    // The minimum speed of the ghost
    let minSpeed: CGFloat = 200
    // Margin for labels
    let MARGIN: CGFloat = 5
    
    
    // An enum for bit masks
    enum GBPhysicsCategory:UInt32 {
        case GBBodyGhost = 1
        case GBBodyBoxman = 2
        case GBBodyPowerup = 4
        case GBBodyBounds = 8
    }
    
    // The ghost
    var ghost: SKSpriteNode = SKSpriteNode()

    // Is the ghost facing left
    var facingLeft: Bool = true
    
    // Motion manager
    var motionManager: CMMotionManager = CMMotionManager()
    // Last vector from accelerometer data
    var lastVector: [Double] = [0.0, 0.0, 0.0]
    
    // Have we started
    var started: Bool = false
    

    var pos = true
    
    // Sensitivity of accelerometer
    var horizontalSensitivity: CGFloat = 0.5
    var verticalSensitivity: CGFloat = 0.5
    
    // Game settings
    var settings = NSUserDefaults()
    
    
    
    // Initializes the scene
    override init(size: CGSize) {
        
        super.init(size: size)
        
        // The center of the screen
        var center: CGPoint = CGPointMake(self.size.width / 2, self.size.height / 2)

        // Initializes the background
        var background: SKSpriteNode = SKSpriteNode(imageNamed: "BackScreen")
        background.position = center
        self.addChild(background)
        
        // Initializes the ghost
        ghost = SKSpriteNode(imageNamed: "ghost")
        ghost.zPosition = 100
        ghost.position = center
        ghost.physicsBody = SKPhysicsBody(circleOfRadius: ghost.frame.size.width / 2.2)
        ghost.physicsBody?.categoryBitMask = GBPhysicsCategory.GBBodyGhost.toRaw()
        ghost.physicsBody?.contactTestBitMask = GBPhysicsCategory.GBBodyBoxman.toRaw()
        ghost.physicsBody?.collisionBitMask = GBPhysicsCategory.GBBodyBounds.toRaw()
        ghost.physicsBody?.allowsRotation = false
        self.addChild(ghost)
        
        
        // Sets up the physics of the screen
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: CGRectMake(BORDER, BORDER, self.size.width - 2 * BORDER, self.size.height - 2 * BORDER))
        self.physicsBody?.categoryBitMask = GBPhysicsCategory.GBBodyBounds.toRaw()
        self.physicsBody?.dynamic = false
        self.physicsWorld.contactDelegate = self
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    
    // Updates the scene
    override func update(currentTime: NSTimeInterval) {
        // Makes the countdown
        if !started {
            var orientation = UIDevice.currentDevice().orientation
            if orientation == UIDeviceOrientation.LandscapeLeft {
                
                if settings.doubleForKey("ayx") > 0 {
                    settings.setDouble(settings.doubleForKey("ayx") * -1, forKey: "ayx")
                }
                
                pos = false
            } else if orientation == UIDeviceOrientation.LandscapeRight {
                
                if settings.doubleForKey("ayx") < 0 {
                    settings.setDouble(settings.doubleForKey("ayx") * -1, forKey: "ayx")
                }
                
                pos = true
            }
            
            started = true
        }

        moveGhost(CGPointZero)
    }
    
    // Moves the ghost
    func moveGhost(relativePosition: CGPoint) {
        var rotationAngle: CGFloat = 0
        
        verticalSensitivity = CGFloat(settings.doubleForKey("verticalSensitivity"))
        horizontalSensitivity = CGFloat(settings.doubleForKey("horizontalSensitivity"))
        
        if motionManager.accelerometerData != nil {
            // Raw acceleration vector
            var raw = [motionManager.accelerometerData.acceleration.x,
                motionManager.accelerometerData.acceleration.y,
                motionManager.accelerometerData.acceleration.z]
            
            raw = lowPass(&raw)
            
            // If there is no motion, stop
            if raw[0] == 0 && raw[1] == 0 && raw[2] == 0 {
                return;
            }
            
            var ay: [Double] = [settings.doubleForKey("ayx"), settings.doubleForKey("ayy"), settings.doubleForKey("ayz")];
            
            
            // The neutral position
            var az = [0.0, 1.0, 0.0]
            var ax = [ay[2], 0.0, -ay[0]]
            
            var rawDotAz = raw[0] * az[0]
            rawDotAz += raw[1] * az[1]
            rawDotAz += raw[2] * az[2]
            
            var rawDotAx = raw[0] * ax[0]
            rawDotAx += raw[1] * ax[1]
            rawDotAx += raw[2] * ax[2]
            
            // The motion vector
            var accel2D = CGPointZero
            if pos {
                accel2D = CGPointMake(CGFloat(rawDotAz), CGFloat(rawDotAx))
            } else {
                accel2D = CGPointMake(CGFloat(-rawDotAz), CGFloat(-rawDotAx))
            }
            
            var steerDeadZone: CGFloat = 0.02
            
            // Filters out little jitters
            if accel2D.x < steerDeadZone && accel2D.x > -steerDeadZone {
                accel2D.x = 0
            }
            if accel2D.y < steerDeadZone && accel2D.y > -steerDeadZone {
                accel2D.y = 0
            }
            
            
            ghost.physicsBody?.velocity = CGVectorMake(2.5 * accel2D.x * (minSpeed + speedRange * horizontalSensitivity), 2.5 * accel2D.y * (minSpeed + speedRange * verticalSensitivity))
            
            // Sets the direction of the ghost with limited rotation
            if !CGPointEqualToPoint(accel2D, CGPointZero) {
                rotationAngle = CGFloat(atan2f(CFloat(accel2D.y), CFloat(accel2D.x)))
            }
        }
        
        // Sets the rotation of the ghost
        if rotationAngle <= CGFloat(M_PI_2) && rotationAngle > -CGFloat(M_PI_2) {
            if facingLeft {
                facingLeft = false
            }
            ghost.yScale = 1.0
            ghost.xScale = -1.0
            ghost.zRotation = rotationAngle / 3
        } else if rotationAngle > CGFloat(M_PI_2) && rotationAngle <= CGFloat(M_PI) {
            if !facingLeft {
                facingLeft = true
            }
            ghost.xScale = -1.0
            ghost.yScale = -1.0
            ghost.zRotation = 2 * CGFloat(M_PI) / 3 + rotationAngle / 3
        } else {
            if !facingLeft {
                facingLeft = true
            }
            ghost.xScale = -1.0
            ghost.yScale = -1.0
            ghost.zRotation = -2 * CGFloat(M_PI) / 3 + rotationAngle / 3
        }
    }
    
    // Damps the gittery motion with a lowpass filter
    func lowPass(inout vector: [Double]) -> [Double] {
        let blend: Double = 0.2
        
        // Smoothens out the data input
        vector[0] = vector[0] * blend + lastVector[0] * (1 - blend)
        vector[1] = vector[1] * blend + lastVector[1] * (1 - blend)
        vector[2] = vector[2] * blend + lastVector[2] * (1 - blend)
        
        // Sets the last vector to be the current one
        lastVector = vector
        
        // Returns the lowpass vector
        return vector
    }
    
    // Resets the game
    func reset() {
        
        ghost.hidden = false
        ghost.position = CGPointMake(self.size.width / 2, self.size.height / 2)
        settings.setBool(false, forKey: "rotateAllowed")
    }
    
    func centerGhost() {
        
        // The center of the screen
        var center: CGPoint = CGPointMake(self.size.width / 2, self.size.height / 2)
        ghost.position = center;
    }
}
