//
//  SettingsViewController.swift
//  Ghostbiters
//
//  Created by Daniel Shaar on 7/2/14.
//  Coded by Daniel Nissenbaum
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit
import Foundation
import CoreMotion

class SettingsViewController: UIViewController {
    
    @IBOutlet var joystickButton: UIButton!
    @IBOutlet var tiltButton: UIButton!
    
    @IBOutlet var verticalSlider: UISlider!
    @IBOutlet var horizontalSlider: UISlider!
    
    @IBOutlet var verticalSliderLabel: UILabel!
    @IBOutlet var horizontalSliderLabel: UILabel!
    
    @IBOutlet var calibrateButton: UIButton!
    
    
    
    //@IBOutlet var FBButton: UIButton!
    
    var settings = NSUserDefaults();
    
    // Motion manager
    let motionManager: CMMotionManager = CMMotionManager()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //self.fbLoginView.delegate = self
        //self.fbLoginView.readPermissions = ["public_profile", "email", "user_friends"]

        // set state of buttons
        tiltButton.selected = !settings.boolForKey("joystickSelected")
        calibrateButton.enabled = !settings.boolForKey("joystickSelected")
        joystickButton.selected = settings.boolForKey("joystickSelected")
        
        verticalSlider.value = settings.floatForKey("verticalSensitivity")
        verticalSliderLabel.text = "\(Int(verticalSlider.value * 100))%"
        
        horizontalSlider.value = settings.floatForKey("horizontalSensitivity")
        horizontalSliderLabel.text = "\(Int(horizontalSlider.value * 100))%"
        
        motionManager.startAccelerometerUpdates()
        
    }

    @IBAction func joystick() {
        
        // set state of buttons
        tiltButton.selected = false
        joystickButton.selected = true
        calibrateButton.enabled = false
        
        // update UserDefault settings
        settings.setBool(true, forKey: "joystickSelected")
    }
    
    @IBAction func tilt () {
        
        // set state of buttons
        joystickButton.selected = false
        tiltButton.selected = true
        calibrateButton.enabled = true
        
        // update UserDefault settings
        settings.setBool(false, forKey: "joystickSelected")
    }
    
    @IBAction func verticalSlide() {
        var newValue = verticalSlider.value;
        settings.setFloat(newValue, forKey: "verticalSensitivity")
        verticalSliderLabel.text = "\(Int(verticalSlider.value * 100))%"
    }
    
    @IBAction func horizontalSlide() {
        var newValue = horizontalSlider.value;
        settings.setFloat(newValue, forKey: "horizontalSensitivity")
        horizontalSliderLabel.text = "\(Int(horizontalSlider.value * 100))%"
    }
    
    @IBAction func restoreDefaults() {
        settings.setBool(false, forKey: "joystickSelected")
        tiltButton.selected = !settings.boolForKey("joystickSelected")
        calibrateButton.enabled = !settings.boolForKey("joystickSelected")
        joystickButton.selected = settings.boolForKey("joystickSelected")
        
        settings.setFloat(0.5, forKey: "verticalSensitivity")
        verticalSlider.value = 0.5
        verticalSliderLabel.text = "\(Int(verticalSlider.value * 100))%"
        
        settings.setFloat(0.5, forKey: "horizontalSensitivity")
        horizontalSlider.value = 0.5
        horizontalSliderLabel.text = "\(Int(horizontalSlider.value * 100))%"
        
        settings.setDouble(0.54, forKey: "ayx")
        settings.setDouble(0.0, forKey: "ayy")
        settings.setDouble(-0.84, forKey: "ayz")
    }
    
    @IBAction func calibrate() {

        var calVC:CalibrationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CalibrationViewController") as CalibrationViewController
        
        self.navigationController?.pushViewController(calVC, animated: true)
    }
    
    
    @IBAction func back() {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // Hides Status Bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}