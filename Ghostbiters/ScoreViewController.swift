//
//  ScoreViewController.swift
//  Ghostbiters
//
//  Created by Daniel Shaar on 7/3/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit
import Foundation

class ScoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var localScores: UITableView!
    
    var scores: [Score] = []

    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == localScores {
            return scores.count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView == localScores {
            var cell: UITableViewCell = localScores.dequeueReusableCellWithIdentifier("firstCell") as UITableViewCell
            
            var index: UILabel = cell.viewWithTag(1) as UILabel
            index.text = "\(indexPath.row + 1)."
            
            var date: UILabel = cell.viewWithTag(2) as UILabel
            date.text = "\(scores[indexPath.row].date)"
            
            var score: UILabel = cell.viewWithTag(3) as UILabel
            score.text = "\(scores[indexPath.row].score)"
            

            
            return cell
        }
        
        return UITableViewCell();
    }

    
    override func viewDidLoad() {
        
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        var path = paths[0].stringByAppendingPathComponent("scores")
        
        if let scr = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as? [Score] {
            scores = scr
            //NSLog("SCORE FOUND!");
        }
        
    }
    
    // Hides Status Bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
