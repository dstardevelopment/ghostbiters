//
//  ScoreView.swift
//  Ghostbiters
//
//  Created by Daniel Nissenbaum on 8/13/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit

class ScoreView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var image: UIImageView!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Initialization code
        nameLabel.adjustsFontSizeToFitWidth = true
        scoreLabel.adjustsFontSizeToFitWidth = true
        update();
        
        
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NSBundle.mainBundle().loadNibNamed("ScoreView", owner: self, options: nil)
        self.addSubview(view)
        update()
    }
    
    func update() {
        
        FBRequest.requestForMe().startWithCompletionHandler({connection, result, error in
            if (error != nil) {
                println("Error in request")
            }
            else if (result != nil) {
                var id = result.objectID
                var name = result.first_name as String
                self.nameLabel.text = name
                
                var settings = NSUserDefaults()
                var highScore = Int(settings.doubleForKey("highScore"));
                
                self.scoreLabel.text = "\(highScore)"
                
                var proPicURL: NSURL = NSURL.URLWithString("https://graph.facebook.com/\(id)/picture?type=large")
                var proPicData = NSData.dataWithContentsOfURL(proPicURL, options: nil, error: nil)
                
                self.image.image = UIImage(data: proPicData);
                
                
            } else {
                NSLog("No Result")
            }
            })
    }
    
}
