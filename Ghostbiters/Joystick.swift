//
//  Joystick.swift
//  Ghostbiters
//
//  Created by Daniel Shaar on 7/3/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit
import GLKit

class Joystick: UIView {
    
    // Relative position of the knob to the base
    var relativePosition: CGPoint = CGPointZero
    
    // The image views for the base and knob
    var baseImageView: UIImageView = UIImageView()
    var knobImageView: UIImageView = UIImageView()
    
    // The center point of the base
    var baseCenter: CGPoint = CGPointZero
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        // Allows for the user to interact with the joystick
        self.userInteractionEnabled = true
        
        // Sets up the base
        baseImageView = UIImageView(frame: self.bounds)
        baseImageView.contentMode = UIViewContentMode.ScaleAspectFill
        baseImageView.image = UIImage(named: "base.png")
        self.addSubview(baseImageView)
        
        // Sets up the knob
        knobImageView = UIImageView(image:UIImage(named: "knob.png"))
        knobImageView.center = baseCenter
        self.addSubview(knobImageView)
        
        // Hides the joystick to start
        baseImageView.hidden = true
        knobImageView.hidden = true
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    // Move the knob to a new position
    func updateKnobWithPosition(position: CGPoint) {
        
        // Gets the distance from the center
        var positionToCenter: CGPoint = CGPointMake(position.x - baseCenter.x, baseCenter.y - position.y)
        var magnitude: CGFloat = CGFloat(sqrtf(CFloat(positionToCenter.x * positionToCenter.x + positionToCenter.y * positionToCenter.y)))
        
        // Deterimnes the direction the knob was dragged in
        var direction:CGPoint
        
        if magnitude == 0 {
            direction = CGPointZero
        } else {
            direction = CGPointMake(positionToCenter.x / magnitude, positionToCenter.y / magnitude)
        }
        
        // Verifies that the user did not break the bounds
        var radius: CGFloat = self.frame.size.width / 2
        
        if magnitude > radius {
            magnitude = radius
            positionToCenter = CGPointMake(direction.x * radius, direction.y * radius)
        }
        
        // The new relative position of the knob
        relativePosition = CGPointMake(direction.x * magnitude/radius, direction.y * magnitude/radius);
        
        // Adjusts the knob image view
        knobImageView.center = CGPointMake(baseCenter.x + positionToCenter.x, baseCenter.y - positionToCenter.y)
    }
    
    // Moves the base to a new position
    func updateBaseWithPosition(position: CGPoint) {
        baseCenter = position
        baseImageView.center = baseCenter
    }
    
    // Shows the joystick when the user is using it
    func joystickHidden(hide: Bool) {
        baseImageView.hidden = hide
        knobImageView.hidden = hide
    }
}
