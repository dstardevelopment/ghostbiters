//
//  LocalScoreTableViewCell.swift
//  Ghostbiters
//
//  Created by Daniel Nissenbaum on 9/13/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit

class LocalScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var index: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
