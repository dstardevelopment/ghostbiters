//
//  HelpViewController.swift
//  Ghostbiters
//
//  Created by Daniel Shaar on 7/2/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit
import Foundation

class HelpViewController: UIViewController {
    
    // The images of the help screens
    let images:[UIImage] = [
        UIImage(named: "help_0.png"),
        UIImage(named: "help_1.png")
    ]
    
    // Keeps track of the current help image
    var currentImage:Int = 0
    
    // Outlets for the imageView and two buttons
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var lastButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    override func viewDidLoad() {
        currentImage = 0
        imageView.image = images[currentImage]
        lastButton.enabled = false
    }
    
    // Method called when the back button is pressed
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // Method called when the last button is pressed
    @IBAction func last() {
        
        switch currentImage {
        
        // If the image is already the first image, return
        case 0:
            lastButton.enabled = false
            nextButton.enabled = true
            return
            
        // If the next image will be the first, disable the last button
        case 1:
            lastButton.enabled = false
            nextButton.enabled = true
            
        // If the image is on the last image, enable the next button when the user hits last
        case (images.count - 1):
            nextButton.enabled = true
            
            
        default:
            break
        }
        
        // Decrement currentImage, and set new image
        imageView.image = images[--currentImage]
    }
    
    // Method called when the next button is pressed
    @IBAction func next() {
        
        switch currentImage {
            
        // If image is already on the last image, return
        case (images.count - 1):
            lastButton.enabled = true
            return
            
        // If the next image will be the last, disable the next button
        case (images.count - 2):
            nextButton.enabled = false
            lastButton.enabled = true
            
        // If the image is on the first image, enable the last button when the usere hits next
        case 0:
            lastButton.enabled = true
            
        default:
            break
        }
        
        // Decrement currentImage, and set new image
        imageView.image = images[++currentImage]
    }
    
    // Hides Status Bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
