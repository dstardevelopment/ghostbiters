//
//  LoginView.swift
//  Ghostbiters
//
//  Created by Daniel Nissenbaum on 8/13/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit

class LoginView: UIView {

    
    @IBOutlet var view: UIView!
    @IBOutlet var button: UIButton!
    
    override init() {
        super.init()
        update()
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        update()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NSBundle.mainBundle().loadNibNamed("LoginView", owner: self, options: nil)
        self.addSubview(view)
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        update()
    }
    
    func update() {
        if button == nil {
            return
        }
        if PFFacebookUtils.isLinkedWithUser(PFUser.currentUser()) {
            button.setImage(UIImage(named: "FBLogout"), forState: UIControlState.Normal)
            button.setImage(UIImage(named: "FBLogout_Selected"), forState: UIControlState.Highlighted)
        } else {
            button.setImage(UIImage(named: "FBLogin"), forState: UIControlState.Normal)
            button.setImage(UIImage(named: "FBLogin_Selected"), forState: UIControlState.Highlighted)
        }
    }
    
    
    @IBAction func buttonPressed() {
        NSLog("\(PFFacebookUtils.isLinkedWithUser(PFUser.currentUser()))")
        if PFFacebookUtils.isLinkedWithUser(PFUser.currentUser()) {
            logOut();
        } else {
            login();
        }
    }
    
    func login() {
        PFFacebookUtils.logInWithPermissions(["public_profile", "email", "user_friends"], {
            (user: PFUser!, error: NSError!) -> Void in
            if user == nil {
                NSLog("Uh oh. The user cancelled the Facebook login.")
            } else if user.isNew {
                NSLog("User signed up and logged in through Facebook!")
                self.button.setImage(UIImage(named: "FBLogout"), forState: UIControlState.Normal)
                self.button.setImage(UIImage(named:"FBLogout_Selected"), forState: UIControlState.Highlighted)
            } else {
                NSLog("User logged in through Facebook!")
                self.button.setImage(UIImage(named: "FBLogout"), forState: UIControlState.Normal)
                self.button.setImage(UIImage(named: "FBLogout_Selected"), forState: UIControlState.Highlighted)
            }
            })
        
        if !PFFacebookUtils.isLinkedWithUser(PFUser.currentUser()) {
            PFFacebookUtils.linkUser(PFUser.currentUser(), permissions:nil, {
                (succeeded: Bool!, error: NSError!) -> Void in
                if succeeded != nil {
                    NSLog("Woohoo, user logged in with Facebook!")
                }
                })
        }
    }
    
    func logOut() {
        PFUser.logOut()
        FBSession.activeSession().closeAndClearTokenInformation()
        button.setImage(UIImage(named: "FBLogin"), forState: UIControlState.Normal)
        button.setImage(UIImage(named: "FBLogin_Selected"), forState: UIControlState.Highlighted)
    }
    
    

}
