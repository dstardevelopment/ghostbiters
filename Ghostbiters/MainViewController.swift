//
//  MainViewController.swift
//  Ghostbiters
//
//  Created by Daniel Shaar on 7/2/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit
import Foundation
import iAd

class MainViewController: UIViewController {
    
    
    //@IBOutlet var proPic: UIImageView!
    
    /******************************* Transitions to another VC *******************************/
    @IBAction func selectMode() {
        var gameVC:GameViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GameViewController") as GameViewController
        
        // Animates the transition to be a flip
        UIView.animateWithDuration(0.75,
            animations: {
                UIView.setAnimationCurve(UIViewAnimationCurve.EaseInOut)
                UIView.setAnimationTransition(UIViewAnimationTransition.FlipFromLeft, forView: self.navigationController!.view, cache: false)
            }
        )
        self.navigationController?.pushViewController(gameVC, animated: true)
    }
    
    @IBAction func viewScores() {
        var scoreVC: ScoreViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ScoreViewController") as ScoreViewController
        self.navigationController?.pushViewController(scoreVC, animated:true)
    }
    
    @IBAction func settings() {
        var settingsVC: SettingsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("SettingsViewController") as SettingsViewController
        self.navigationController?.pushViewController(settingsVC, animated:true)
    }
    
    @IBAction func help() {
        var helpVC: HelpViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HelpViewController") as HelpViewController
        self.navigationController?.pushViewController(helpVC, animated:true)
    }
    
    @IBAction func about() {
        var aboutVC: AboutViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AboutViewController") as AboutViewController
        self.navigationController?.pushViewController(aboutVC, animated:true)
    }
    /*****************************************************************************************/

//    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
//        UIView.animateWithDuration(0.5, animations: {
//            //banner.alpha = CGFloat(0.0)
//            banner.hidden = true
//
//        })
//        NSLog("Ad not found")
//    }
//    
//    func bannerViewDidLoadAd(banner: ADBannerView!) {
//        UIView.animateWithDuration(0.5, animations: {
//            //banner.alpha = CGFloat(1.0)
//            banner.hidden = false
//        })
//        NSLog("Ad found")
//
//    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
//        PFFacebookUtils.logInWithPermissions(["public_profile", "email", "user_friends"], {
//            (user: PFUser!, error: NSError!) -> Void in
//            if user == nil {
//                NSLog("Uh oh. The user cancelled the Facebook login.")
//            } else if user.isNew {
//                NSLog("User signed up and logged in through Facebook!")
//            } else {
//                NSLog("User logged in through Facebook!")
//            }
//            })
//        
//        if FBSession.activeSession()!.isOpen {
//            if !PFFacebookUtils.isLinkedWithUser(PFUser.currentUser()) {
//                PFFacebookUtils.linkUser(PFUser.currentUser(), permissions:nil, {
//                    (succeeded: Bool!, error: NSError!) -> Void in
//                    if succeeded != nil {
//                        NSLog("Woohoo, user logged in with Facebook!")
//                    }
//                })
//            }
//        }
        
        
        var settings = NSUserDefaults();
        var defaults: [NSObject:NSObject] = ["joystickSelected":false, "ayx":0.54, "ayy":0.0, "ayz":-0.84, "horizontalSensitivity":0.5, "verticalSensitivity":0.5, "highScore":0.0]
        settings.registerDefaults(defaults)
    
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        var path = paths[0].stringByAppendingPathComponent("scores")
        
        if let scores: AnyObject = NSKeyedUnarchiver.unarchiveObjectWithFile(path) {
            
        } else {
            var scr: [Score] = [] 
            NSKeyedArchiver.archiveRootObject(scr, toFile: path)
        }

    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)

//        if PFFacebookUtils.isLinkedWithUser(PFUser.currentUser()) {
//            var id = ""
//            
//            FBRequest.requestForMe().startWithCompletionHandler({connection, result, error in
//                if (error != nil) {
//                    println("Error in request")
//                    self.proPic.hidden = true
//                }
//                else if (result != nil) {
//                    id = result.objectID
//                    //NSLog("\(id)")
//                    var proPicURL: NSURL = NSURL.URLWithString("https://graph.facebook.com/\(id)/picture?type=large")
//                    //NSLog("\(proPicURL)")
//                    var proPicData = NSData.dataWithContentsOfURL(proPicURL, options: nil, error: nil)
//                    
//                    self.proPic.image = UIImage(data: proPicData);
//                    self.proPic.hidden = false
//                    
//                    
//                } else {
//                    NSLog("No Result")
//                    self.proPic.hidden = true
//                }
//                })
//            
//        } else {
//            proPic.hidden = true
//        }
    }
    
    // Hides Status Bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    

}