//
//  GameViewController.swift
//  Ghostbiters
//
//  Created by Daniel Shaar on 7/1/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit
import SpriteKit
import CoreMotion
import iAd

class GameViewController: UIViewController {
    
    // The scorebar height
    let SCOREBAR: CGFloat = 100
    
    // The game scene
    var scene: GameScene = GameScene(size:CGSizeZero)
    // The joystick
    var joystick: Joystick = Joystick(frame:CGRectMake(0, 0, 130, 130))
    
    // The game over menu
    @IBOutlet var gameOverView: UIView!
    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var highScoreLabel: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var loginView: LoginView!
    
    @IBOutlet var pauseButton: UIButton!
    @IBOutlet var pauseMenu: UIView!
    
    var validPress: Bool = false
    
    // The acceleromoter manager
    var motionManager: CMMotionManager = CMMotionManager()
    var touchesAllowed: Bool = false
    
    @IBOutlet weak var scoreView: ScoreView!
    
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        
        gameOverView.hidden = true
        
        pauseMenu.hidden = true
        pauseMenu.center = CGPointMake(512.5, 383.5);
        
        // The view of the game
        var skView: SKView = self.view as SKView
        
        // FPS and Node Count for debugging
        skView.showsFPS = false
        skView.showsNodeCount = false
        
        // The scene in which gameplay will take place
        scene = GameScene.sceneWithSize(skView.bounds.size)
        scene.scaleMode = SKSceneScaleMode.AspectFill
        scene.joystickControl = joystick
        scene.gameOverView = gameOverView
        scene.finalScoreLabel = scoreLabel
        scene.highScoreLabel = highScoreLabel
        //scene.scrollView = scrollView
        //scene.loginView = loginView
        scene.pauseMenu = pauseMenu
        
        // Presents the scene
        skView.presentScene(scene)
        
        touchesAllowed = NSUserDefaults().boolForKey("joystickSelected")
        scene.joystick = touchesAllowed
        
        if !touchesAllowed {
            // Sets up accelerometer motion
            motionManager.accelerometerUpdateInterval = 0.05
            motionManager.startAccelerometerUpdates()
            scene.motionManager = motionManager
        }
        // Sets up the joystick
        self.view.addSubview(joystick)
    }
    
    override func viewDidLoad() {
        
        // load user settings
        var settings = NSUserDefaults()
        
        // set touchesAllowed
        touchesAllowed = settings.boolForKey("joystickSelected")
        settings.setBool(touchesAllowed, forKey: "rotateAllowed")
        
    }
    
    override func shouldAutorotate() -> Bool {
        var settings = NSUserDefaults();
        return settings.boolForKey("rotateAllowed")
    }
    
    /*************************************TOUCH HANDLER**************************************/
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        if  touchesAllowed && !joystick.hidden {
            var touchLocation: CGPoint = touches.anyObject()!.locationInView(self.view)
            let padSize: CGFloat = 50
            
            if touchLocation.x < padSize ||
                touchLocation.x > self.view.frame.size.width - padSize ||
                touchLocation.y < padSize ||
                touchLocation.y > self.view.frame.size.height - padSize {
                    validPress = false
                    return;
            }
            
            validPress = true
            scene.ghost.physicsBody?.linearDamping = 0.1
            joystick.updateBaseWithPosition(touchLocation)
            joystick.updateKnobWithPosition(touchLocation)
            joystick.joystickHidden(false)
        }
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        if validPress && !joystick.hidden {
            var touchLocation:CGPoint = touches.anyObject()!.locationInView(self.view)
            joystick.updateKnobWithPosition(touchLocation)
            scene.moveGhost(joystick.relativePosition)
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        joystick.joystickHidden(true)
        joystick.relativePosition = CGPointZero
        scene.ghost.physicsBody?.linearDamping = 0.8
    }
    
    override func touchesCancelled(touches: NSSet, withEvent event: UIEvent) {
        joystick.joystickHidden(true)
        joystick.relativePosition = CGPointZero
        scene.ghost.physicsBody?.linearDamping = 0.8
    }
    
    // Hides Status Bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // Resets the scene
    @IBAction func retry() {
        scene.reset()
        
        var settings = NSUserDefaults();
        settings.setBool(touchesAllowed, forKey: "rotateAllowed")
    }
    
    // Goes back to the main menu
    @IBAction func menu() {
        
        //"Are you sure you want to give up and go back to the main menu? You score will not be saved!"
        
        // Animates the transition to be a flip
        UIView.animateWithDuration(0.75,
            animations: {
                UIView.setAnimationCurve(UIViewAnimationCurve.EaseInOut)
                UIView.setAnimationTransition(UIViewAnimationTransition.FlipFromRight, forView: self.navigationController!.view, cache: false)
            }
        )
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    // Pauses the game.
    @IBAction func pause() {
        if gameOverView.hidden == false {
            return
        }
        scene.pause()
    }
    // Resumes the game.
    @IBAction func resume() {
        scene.resume()
        touchesAllowed = NSUserDefaults().boolForKey("joystickSelected")
        joystick.hidden = !NSUserDefaults().boolForKey("joystickSelected")
    }
    
    @IBAction func settings() {
        var settingsVC: SettingsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("SettingsViewController") as SettingsViewController
        self.navigationController?.pushViewController(settingsVC, animated:true)
    }
}
