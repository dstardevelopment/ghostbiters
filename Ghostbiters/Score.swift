//
//  ScoreStruct.swift
//  Ghostbiters
//
//  Created by Daniel Nissenbaum on 9/13/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import Foundation


class Score: NSObject {
    var score = 0
    var date = ""
    

    
    init(scr:Int) {
        score = scr
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy ' ' h:mm a" // superset of OP's format
        date = dateFormatter.stringFromDate(NSDate())
        
    }
    
    func encodeWithCoder(encoder: NSCoder) {
        encoder.encodeInteger(score, forKey: "score")
        encoder.encodeObject(date, forKey: "date")
    }
    
    func initWithCoder(decoder: NSCoder) -> Score {
        self.score = decoder.decodeIntegerForKey("score") as Int
        self.date = decoder.decodeObjectForKey("date") as String
        return self
    }
    
    init(coder aDecoder: NSCoder!) {
        self.score = aDecoder.decodeIntegerForKey("name") as Int
        self.date = aDecoder.decodeObjectForKey("date") as String
    }

}