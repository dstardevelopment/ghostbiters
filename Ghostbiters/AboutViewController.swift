//
//  AboutViewController.swift
//  Ghostbiters
//
//  Created by Daniel Shaar on 7/2/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit
import Foundation

class AboutViewController: UIViewController {
    
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // Hides Status Bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
