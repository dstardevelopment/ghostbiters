//
//  CalibrateViewController.swift
//  Ghostbiters
//
//  Created by Daniel Nissenbaum on 8/12/14.
//  Copyright (c) 2014 D* Development. All rights reserved.
//

import UIKit
import SpriteKit
import CoreMotion

class CalibrationViewController: UIViewController {
    
    // The calibration scene
    var scene: CalibrationScene = CalibrationScene(size:CGSizeZero)

    var validPress: Bool = false
    
    // The acceleromoter manager
    var motionManager: CMMotionManager = CMMotionManager()

    var settings = NSUserDefaults();
        
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        
        // The view of the game
        let skView = self.view as SKView
        
        // FPS and Node Count for debugging
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        // The scene in which gameplay will take place
        scene = CalibrationScene.sceneWithSize(skView.bounds.size)
        scene.scaleMode = SKSceneScaleMode.AspectFill
        
        // Presents the scene
        skView.presentScene(scene)

        // Sets up accelerometer motion
        motionManager.accelerometerUpdateInterval = 0.05
        motionManager.startAccelerometerUpdates()
        scene.motionManager = motionManager
    }
    
    override func viewDidLoad() {
        motionManager.startAccelerometerUpdates()

    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }

    
    // Hides Status Bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // Resets the scene
    @IBAction func retry() {
        scene.reset()
    }
    
    @IBAction func calibrate() {
        if (motionManager.accelerometerAvailable) {
            var raw = [motionManager.accelerometerData.acceleration.x,
                motionManager.accelerometerData.acceleration.y,
                motionManager.accelerometerData.acceleration.z]
            //NSLog("\(raw[0]), \(raw[1]), \(raw[2]), ")
            
            self.settings.setDouble(raw[0], forKey: "ayx")
            self.settings.setDouble(raw[1], forKey: "ayy")
            self.settings.setDouble(raw[2], forKey: "ayz")
        }
        
        scene.centerGhost();
        
    }
    
    // Goes back to the main menu
    @IBAction func back() {
        motionManager.stopAccelerometerUpdates()
        self.navigationController?.popViewControllerAnimated(true)
    }
}
